from pygments.lexer import RegexLexer, Lexer, words
from pygments.token import Name
from prompt_toolkit.lexers import PygmentsLexer


class OrgasmLexer(Lexer):
    def lex_document(self, document):
        return PygmentsLexer(CLILexer, sync_from_start=False).lex_document(document)


class CLILexer(RegexLexer):
    wordlist = ("set",
                "use",
                "options",
                "help"
                )

    tokens = {
        "root": [
            (words(wordlist, suffix=r"\b"), Name.Builtin, "command"),
            (r"\w+", Name),
        ],
        "command": [
            (r"\w+", Name),
        ]
    }