from lib.exception import MissingRequired


class Options:
    def __init__(self):
        self.values = {}
        self.required = []
        self.owned = []

    def has(self, key):
        if key in self.values:
            return True

        return False

    def add(self, key, value=None, type=str, change_me=True, required=False, public=True, scoped=True):
        self.values[key] = Option(key, value, type, change_me, required, public, scoped)

        if required:
            self.set_required(key)

        self.owned.append(key)

    def add_from_option(self, opt):
        opt.inherited = True
        self.values[opt.key] = opt
        if opt.required:
            self.required.append(opt.key)

    def set(self, key, val):
        self.values[key].val = val
        self.values[key].is_set = True

    def to_dict(self):
        values_dict = {}
        for opt in self.values.values():
            values_dict[opt.key] = opt.val

        return values_dict

    def get(self, key):
        return self.values[key].val

    def get_type(self, key):
        return self.values[key].type

    def get_option(self, key):
        return self.values[key]

    def get_options(self):
        return [self.values[name] for name in self.values]

    def set_required(self, key):
        self.required.append(key)

    def check_required(self):
        for key in self.required:
            if not self.values[key].is_set:
                raise MissingRequired(key)

    def add_patcher_options(self, patcher):
        values = patcher.get_options().values
        for opt in values.values():
            self.owned.append(opt.key)
            if opt.required:
                self.set_required(opt.key)

        self.values.update(values)


class Option:
    def __init__(self, key, value=None, type=str, change_me=True, required=False, public=True, scoped=True):
        self.key = key
        self.val = value
        self.default = value
        self.change_me = change_me
        self.required = required
        self.type = type
        self.public = public
        self.scoped = scoped

        if not change_me:
            self.is_set = True
        else:
            self.is_set = False

    def cast(self, val):
        return self.type(val)
