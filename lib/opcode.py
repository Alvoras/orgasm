class Memory32:
    def __init__(self):
        self.NOP = [0x90]
        self.PUSH = [0x68]


class Memory64:
    def __init__(self):
        self.NOP = [0x90]
        self.PUSH_RBX = [0x53]
        self.MOVABS = [0x48, 0xbb]


memory32 = Memory32()
memory64 = Memory64()
