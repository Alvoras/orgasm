import os
import platform

from lib.plugin import load_plugin
from lib.options import Options
from lib.utils import list_python_files

from prompt_toolkit.completion import NestedCompleter, DynamicCompleter, Completion


class State:
    def __init__(self, completer):
        self.settings = Options()
        self.settings.add("os", value=platform.system(), required=True, change_me=False, scoped=False)
        self.settings.add("arch", value="32", required=True, change_me=False, scoped=False)
        self.settings.add("verbose", value=False, required=True, change_me=False, scoped=False)
        self.settings.add("clipboard", value=False, required=True, change_me=False, public=False)
        self.settings.add("hex_prefix", value=r"\x", required=True, change_me=False, scoped=False)
        self.settings.add("no_badchars", value=False, type=bool, required=True, change_me=False, scoped=False)
        self.settings.add("optimization_level", value=2, type=int, required=True, change_me=False, scoped=False)
        self.settings.add("badchars_max_loop", value=100, type=int, required=True, change_me=False, scoped=False)
        self.settings.add("badchars", value=[r"00", r"ff", r"0a", r"0d"], type=list, required=True, change_me=False,
                          scoped=False)

        self.sc = ""

        self.loaded_encoder = []
        self.loaded_payload = []
        self.loaded_script = []

        self.payload = None
        self.encoder = None
        self.completer = completer

        self.load_encoder_completion()
        self.load_payload_completion()
        self.load_script_completion()

        self.completer.options["set"] = SetOptionsCompleter(self.get_options_keys)

        self.set_plugin("modules/payload/raw.py", "payload", self)
        self.set_plugin("modules/encoder/raw.py", "encoder", self)

    def load_payload_completion(self):
        completer_data = {}
        payload_paths = list_python_files("modules/payload", blacklist=["base.py"])
        for path in payload_paths:
            completer_data[path] = {}

        new_completer = NestedCompleter.from_nested_dict(completer_data)

        self.completer.options["use"].options["payload"] = new_completer
        self.loaded_payload = payload_paths

    def load_encoder_completion(self):
        completer_data = {}
        encoder_paths = list_python_files("modules/encoder", blacklist=["base.py"])
        for path in encoder_paths:
            completer_data[path] = {}

        new_completer = NestedCompleter.from_nested_dict(completer_data)

        self.completer.options["use"].options["encoder"] = new_completer
        self.loaded_encoder = encoder_paths

    def load_script_completion(self):
        completer_data = {}
        script_paths = list_python_files("modules/script", blacklist=["base.py"], ext=".osf")
        for path in script_paths:
            completer_data[path] = {}

        new_completer = NestedCompleter.from_nested_dict(completer_data)

        self.completer.options["use"].options["script"] = new_completer
        self.loaded_script = script_paths

    def get_options_keys(self):
        options = self.settings.to_dict()
        if self.payload:
            options.update(self.payload.options.to_dict())
        elif self.encoder:
            options.update(self.encoder.options.to_dict())

        return options.keys()

    def reset_option(self, key):
        key = key.lower()
        if self.settings.has(key):
            opt = self.settings.get_option(key)
            if opt.change_me:
                opt.is_set = False
            opt.val = self.settings.get_option(key).default
        elif self.payload and self.payload.options.has(key):
            opt = self.payload.options.get_option(key)
            if opt.change_me:
                opt.is_set = False
            opt.val = self.payload.options.get_option(key).default
        elif self.encoder and self.encoder.options.has(key):
            opt = self.encoder.options.get_option(key)
            if opt.change_me:
                opt.is_set = False
            opt.val = self.encoder.options.get_option(key).default
        else:
            print("Unknown option :", key)
            return

        print(f"{opt.key} => {opt.val}")

    def set_option(self, key, value):
        key = key.lower()

        if self.settings.has(key):
            if self.settings.get_type(key) == list:
                value = value.split(",")

            if self.settings.get_type(key) is not type(value):
                if self.settings.get_type(key) is bool:
                    if value.lower() == "true":
                        value = True
                    elif value.lower() == "false":
                        value = False
                    else:
                        print("Invalid value (bool expected)")
                        return
                value = self.settings.get_option(key).cast(value)

            self.settings.set(key, value)
        elif self.payload and self.payload.options.has(key):
            if self.payload.options.get_type(key) == list:
                value = value.split(",")

            if self.payload.options.get_type(key) is not type(value):
                value = self.payload.options.get_option(key).cast(value)

            self.payload.options.set(key, value)
        elif self.encoder and self.encoder.options.has(key):
            if self.encoder.options.get_type(key) == list:
                value = value.split(",")

            if self.encoder.options.get_type(key) is not type(value):
                value = self.encoder.options.get_option(key).cast(value)

            self.encoder.options.set(key, value)
        else:
            print("Unknown option :", key)
            return

        print(f"{key} => {value}")

    def set_plugin(self, path, category, state):
        if not os.path.isfile(path):
            print(f"{category.title()} {path} not found")
            return

        plugin = load_plugin(path, category)
        setattr(self, category, plugin(state))

    # def set_payload(self, path, state):
    #     if not os.path.isfile(path):
    #         print(f"Payload {path} not found")
    #         return
    #
    #     payload = load_plugin(path)
    #     self.payload = payload(state)
    #
    # def set_encoder(self, path, state):
    #     if not os.path.isfile(path):
    #         print(f"Encoder {path} not found")
    #         return
    #
    #     encoder = load_plugin(path)
    #     self.encoder = encoder(state)

    def options_to_text(self, category):
        text = []
        mod = getattr(self, f"{category}")
        if not mod:
            return "Nothing selected"

        if category == "settings":
            options = self.settings
        else:
            text.append(f"name: {mod.name}")
            options = mod.options

        for opt in options.get_options():
            if opt.public and opt.key in options.owned:
                text.append(
                    f"{'*' if opt.key in options.required and (opt.change_me and not opt.is_set) else ''}{opt.key}: {str(opt.val)}")

            # if options[key]:
            #     val = options[key]
            # elif type(options[key]) is not bool:
            #     val = "<empty>"
            # else:
            #     val = str(options[key])

        return "\n".join(text)


class SetOptionsCompleter(DynamicCompleter):
    def __init__(self, get_options_fn, ignore_case=True, match_middle=True):
        super(SetOptionsCompleter, self).__init__(get_options_fn)
        self.match_middle = match_middle
        self.ignore_case = ignore_case
        self.get_options_fn = get_options_fn

    def get_completions(self, document, complete_event):
        words = self.get_options_fn()
        word_before_cursor = document.get_word_before_cursor()

        # Ignore case = true
        word_before_cursor = word_before_cursor.lower()

        def word_matches(word):
            """ True when the word before the cursor matches. """
            if self.ignore_case:
                word = word.lower()

            if self.match_middle:
                return word_before_cursor in word
            else:
                return word.startswith(word_before_cursor)

        for word in words:
            if word_matches(word):
                yield Completion(word, -len(word_before_cursor))
