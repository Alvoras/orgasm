import os
import subprocess
import shlex

from lib.utils import escape_hex_string


def compile_and_run(options, sc):
    def verbose(text):
        if options.get("verbose"):
            print(text)

    template = """
    #include <sys/mman.h>
    #include <errno.h>
    #include <malloc.h>
    #include <stdio.h>
    #include <string.h>
    
    int main(int argc, char const *argv[])
    {
        char shellcode[] = "__SHELLCODE__";
        int len = (sizeof(shellcode) > 2048) ?sizeof(shellcode):2048;
        
        char *target = (char *) memalign(4096, len);
        mprotect(target, len, PROT_READ | PROT_WRITE | PROT_EXEC);
        memcpy(target, shellcode, len);
        (*(void (*)()) target)();
    
        return 0;
    }
    """

    bootstrap_out = "bs.tmp"
    x86_modifier = "-m32"
    sec_gcc_modifiers = [
        "-fno-stack-protector",
        "-z execstack",
        "-z norelro"
    ]

    if options.get("arch") == "64":
        x86_modifier = ""

    verbose(f"[+] Shellcode : {sc}")
    sc = escape_hex_string(sc.replace(options.get("hex_prefix"), ""))

    patched = template.replace("__SHELLCODE__", sc)

    verbose(f"[+] Writing bootstrap program :\n{patched}")

    with open(f"./{bootstrap_out}.c", "wb") as outfile:
        outfile.write(patched.encode())

    gcc_line = f"gcc ./{bootstrap_out}.c {x86_modifier} -Wall {' '.join(sec_gcc_modifiers)} -o ./{bootstrap_out}"
    verbose(f"[+] Compiling with :\n{gcc_line}")
    p = subprocess.Popen(f"{gcc_line}", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, executable="/bin/bash")
    out, err = p.communicate()
    if out:
        print(out)

    if err:
        print(err)
        try:
            os.unlink(f"{bootstrap_out}")
        except FileNotFoundError:
            pass
        try:
            os.unlink(f"{bootstrap_out}.c")
        except FileNotFoundError:
            pass
        return

    print()
    print("=== start of output ===")
    print()
    os.system(f"./{bootstrap_out}")
    print()
    print("=== end of output ===")
    print()

    os.unlink(f"{bootstrap_out}")
    os.unlink(f"{bootstrap_out}.c")
