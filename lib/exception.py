class UnsupportedArch(Exception):
    def __init__(self, *args):
        self.arch = None
        if args:
            self.arch = args[0]

    def __str__(self):
        if not self.arch:
            return "unsupported architecture"
        else:
            return f"unsupported architecture : {self.arch}"


class UnsupportedPatcherInput(Exception):
    def __init__(self, *args):
        self.name = ""
        self.input_type = ""
        if args:
            self.name = args[0]
            self.input_type = args[1]

    def __str__(self):
        if not self.name and not self.input_type:
            return "unsupported patcher input type"
        else:
            return f"unsupported patcher input type \"{self.input_type}\" for patcher \"{self.name}\""


class MissingRequired(Exception):
    def __init__(self, *args):
        self.missing = None
        if args:
            self.missing = args[0]

    def __str__(self):
        if not self.missing:
            return "a required option is missing"
        else:
            return f"required option \"{self.missing}\" is missing"


class MarkerNotFound(Exception):
    def __init__(self, *args):
        self.missing = None
        if args:
            self.missing = args[0]

    def __str__(self):
        if not self.missing:
            return "the marker is missing"
        else:
            return f"marker \"{self.missing}\" is missing"


class InvalidShellcode(Exception):
    def __init__(self, *args):
        self.sc = None
        if args:
            self.sc = args[0]

    def __str__(self):
        if not self.sc:
            return "invalid shellcode (unspecified)"
        else:
            return f"invalid shellcode : \"{self.sc}\""


