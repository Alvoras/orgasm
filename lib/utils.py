import os
import platform
import subprocess

import pyperclip


def list_python_files(start, ext=".py", blacklist=[]):
    paths = []

    for root, dirs, files in os.walk(start):
        for file in files:
            if file in blacklist:
                continue
            if file.endswith(ext):
                paths.append(os.path.join(root, file))

    return paths


def sc_len(sc, prefix=r"\x"):
    sc = sc.strip("\"")

    try:
        if prefix in sc:
            hex_sc = escaped_string_to_hex(sc)
        else:
            hex_sc = bytearray.fromhex(sc)
    except ValueError:
        print("Invalid shellcode", sc)
        return -1

    return len(hex_sc)


def escaped_string_to_hex(x_str, prefix="\\x"):
    sc = x_str.replace(prefix, " ")
    return bytearray.fromhex(sc)


def hex_to_escaped_string(sc, prefix="\\x"):
    return ''.join(prefix + format(x, "02x") for x in sc)


def escape_hex_string(sc, prefix="\\x"):
    chunks = [sc[i:i + 2] for i in range(0, len(sc), 2)]
    return prefix + prefix.join(chunks)


def check_clipboard():
    try:
        pyperclip.paste()
    except pyperclip.PyperclipException as e:
        print(e)
        print("[!] The clipboard automation has been disabled : copy/paste mechanism not found on your system")
        return False

    return True


def print_loaded(state):
    print(f"[+] Loaded {len(state.loaded_encoder)} encoders")
    print(f"[+] Loaded {len(state.loaded_payload)} payloads")
    print(f"[+] Loaded {len(state.loaded_script)} scripts")


def validate_sc_template(template, prefix="\\x"):
    template = template.replace(prefix, "")
    for idx, chunk in enumerate(template.split("$")):
        if idx % 2 != 0:
            continue

        try:
            bytearray.fromhex(chunk)
        except ValueError as e:
            return e, False

    return "", True


def compile_path_to_shellcode(path, arch, opti_level, clipboard=False, hex_prefix=r"\x"):
    if platform.system() != "Linux":
        print("The compile command is not supported on non-linux platforms")
        return

    path = os.path.abspath(path)

    optimization_flag = "-O2" if opti_level >= 2 else f"-O{str(opti_level)}"

    build_sc_cmd = rf"""nasm {optimization_flag} -felf{arch} {path} -o tmp.o && ld {'-m elf_i386' if arch == '32' else ''} tmp.o -o tmp"""
    nasm_out, err = subprocess.Popen(build_sc_cmd, shell=True, executable="/bin/bash", stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE).communicate()
    if err:
        # os.unlink(f"tmp")
        # os.unlink(f"tmp.o")
        return "", nasm_out, err

    extract_sc_cmd = fr"""for i in `objdump -d tmp | tr '\t' ' ' | tr ' ' '\n' | egrep '^[0-9a-f]{{2}}$' ` ; do echo -En "{hex_prefix}$i" ; done;echo"""
    out, err = subprocess.Popen(extract_sc_cmd, shell=True, executable="/bin/bash", stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE).communicate()

    os.unlink(f"tmp")
    os.unlink(f"tmp.o")

    if out:
        if clipboard:
            pyperclip.copy(out.decode("utf-8"))
            print("Shellcode has been copied to clipboard")

        return out.decode("utf-8"), nasm_out, ""

    if err:
        return "", nasm_out, err


def compile_source_to_shellcode(source, arch, optimization_level, clipboard=False, hex_prefix=r"\x"):
    path = "./tmp.nasm"

    try:
        with open(path, "w") as f:
            f.write(source)
    except Exception as e:
        return "", "", str(e)

    out, nasm_out, err = compile_path_to_shellcode(path, arch, optimization_level, clipboard, hex_prefix=hex_prefix)

    os.unlink("./tmp.nasm")
    return out, nasm_out, err


def has_badchars(badchars, sc):
    for bc in badchars:
        for idx, b in enumerate(sc):
            if bc == b:
                return True

    return False
