import random


def junk():
    alterations = [
        ["push edi", "pop edi"],
        ["inc esi", "dec esi"]
    ]
    rng = random.randint(0, len(alterations)-1)

    return "\n".join(alterations[rng])


def zero(reg):
    alterations = [
        [f"xor {reg}, {reg}"],
        [f"mov {reg}, 0"]
    ]
    rng = random.randint(0, len(alterations)-1)

    return alterations[rng][0]
