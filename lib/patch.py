class Patches:
    def __init__(self):
        self.patches = {}

    def register_patch(self, name, val):
        self.patches[name] = Patch(name, val)

    def get_by_name(self, name):
        try:
            return self.patches[name]
        except KeyError:
            return None


class Patch:
    def __init__(self, name, data):
        self.name = name
        self.data = data

    def compute(self):
        return self.data
