import hashlib
import os
import random
import platform
import subprocess

from crayons import red, white, green
import pyperclip

from lib.utils import sc_len, hex_to_escaped_string, escaped_string_to_hex, has_badchars, compile_path_to_shellcode
from lib.bootstrap import compile_and_run
from lib.exception import MissingRequired, UnsupportedArch
from lib.options import Options


class Orgasm:
    def __init__(self, session, state):
        self.flowers = {"whiteflower": "💮",
                        "sunflower": "🌻",
                        "bouquet": "💐",
                        "cherryblossom": "🌸",
                        "tulip": "🌷",
                        "rose": "🌹",
                        "hibiscus": "🌺",
                        "blossom": "🌼"
                        }

        self.flowers_names = list(self.flowers.keys())
        self.flower = self.flowers[self.flowers_names[random.randint(0, len(self.flowers_names) - 1)]]
        self.state = state
        self.session = session

    def run(self):
        while True:
            try:
                line = self.session.prompt(f"{self.flower} ")
                if not line:
                    continue
            except KeyboardInterrupt:
                continue
            except EOFError:
                break
            else:
                self.process(line)

        print("Méfait accompli.")

    def process(self, line):
        line_chunks = line.strip().split(" ")
        command = line_chunks[0]
        fn_name = f"do_{command}"

        try:
            getattr(self, fn_name)(line_chunks[1:])
        except AttributeError as e:
            print(e)
            print("Unknown command :", command)

    def exec(self, script_path):
        self.do_script([script_path])
        exit(0)

    def do_theme(self, line):
        if len(line) < 1:
            for name, emoji in self.flowers.items():
                print(f"- {emoji}: {name}")

            return

        theme = line[0]
        if theme == "random":
            self.flower = self.flowers[self.flowers_names[random.randint(0, len(self.flowers_names) - 1)]]
        elif theme == "list":
            for name, emoji in self.flowers.items():
                print(f"- {emoji}: {name}")
        else:
            try:
                self.flower = self.flowers[theme]
            except KeyError:
                print(f"Unknown theme \"{theme}\"")

    def do_help(self, line):
        help_msg = r"""
    * set
        Set a parameter value to the given key
        Usage : set <option> <value>
        Example : set offset 0

    * reset
        Reset a parameter to its default value
        Usage : reset <option>
        Example : reset offset

    * reload
        Reload the completion database for the specified tool
        Usage : reload [encoder|payload|script]
        Exemple : reload encoder

    * render
        Build and print the shellcode with the current engines
        Usage : render
        Example : render

    * details, check
        Build the shellcode and show multiple infos on length and badchars
        Usage : details
        Example : details

    * compile
        Build a nasm source file and extract its shellcode. The path can be either absolute or relative to the script's path
        Usage : compile <path/to/file>
        Example : compile examples/compile/main.asm

    * script
        Read a .osf file which contains a thread of instruction to be executed
        Usage : script <path>
        Example : script modules/script/caesar.osf

    * use
        Select the encoder or the payload engine to use
        Usage : use [encoder|payload] <path>
        Example : use encoder modules/encoder/caesar.py

    * try, run
        Build and run the generated shellcode
        Usage : try
        Example : try

    * options, show
        Show the current engines' parameters
        Usage : options
        Example : options

    * search
        Search the specified term in the given categories' paths
        Usage : search [encoder|payload|script] <term>
        Example : search script caesar

    * len
        Give the len of the specified shellcode
        Usage : len <shellcode>
        Example : len "\x31\xc0\x31\xdb\x31"

    * encode
        Encode the specified shellcode with the current engines
        Usage : encode <shellcode>
        Example : encode "\x32\xc1\x32\xdc\x32\xca..."

    * decode
        Decode the specified shellcode with the current engines
        Usage : decode <shellcode>
        Example : decode "\x34\xc2\x34\xdd\x33\xcb..."

    * theme
        Set the cursor to a new flower
        Usage : theme [list|random|<name>]
    """
        print(help_msg)

    def do_set(self, line):
        usage = "Usage : set <option> <key>"
        if not len(line) > 1:
            print(usage)
            return

        self.state.set_option(line[0], " ".join(line[1:]))

    def do_reset(self, line):
        usage = "Usage : reset <option>"
        if not len(line):
            print(usage)
            return

        option_name = line[0]

        self.state.reset_option(option_name)

    def do_use(self, line):
        usage = f"Usage : use [encoder|payload] <path>"
        if len(line) < 2:
            print(usage)
            return

        category = line[0].lower()
        path = line[1].lower()

        if category in ["encoder", "payload"]:
            self.state.set_plugin(path, category, self.state)
            # self.state.set_encoder(path, self.state)
        else:
            print(usage)

    def do_show(self, line):
        self.do_options(line)

    def do_options(self, line):
        print(f"""=== Globals ===
{self.state.options_to_text("settings")}

=== Encoder ===
{self.state.options_to_text("encoder")}

=== Payload ===
{self.state.options_to_text("payload")}
""")

    def do_reload(self, line):
        usage = "Usage : reload [encoder|payload|script]"
        if not len(line) > 0:
            print(usage)
            return

        category = line[0]
        print(f"Reloading {category} plugins...")
        getattr(self.state, f"load_{category}_completion")()
        # progress
        print("OK")

    def do_details(self, line):
        self.do_check(line)

    def do_check(self, line):
        no_encoder = False

        # if not self.state.encoder:
        #     print("Encoder not selected, continuing with raw payload")
        #     no_encoder = True
        #
        # if not self.state.payload:
        #     print("Select a payload first")
        #     return
        #
        # try:
        #     rendered = self.state.payload.render()
        # except (MissingRequired, UnsupportedArch) as e:
        #     print(f"Error while patching payload : {e}")
        #     return
        #
        # if not no_encoder:
        #     try:
        #         rendered = self.state.encoder.render(self.state.payload)
        #     except (MissingRequired, UnsupportedArch) as e:
        #         print(f"Error while patching encoder : {e}")
        #         return
        #
        # if not rendered:
        #     print(f"Unknown error while building shellcode")
        #     return
        #
        # bc_found = False
        #
        # badchars = self.state.settings.get("badchars")
        # split_sc = rendered.split(self.state.settings.get("hex_prefix"))[1:]
        #
        # print(white("Built shellcode :", bold=True))
        # print(rendered)
        # print()

        if not self.state.sc:
            print("Use \"render\" first to generate the shellcode")
            return

        bc_found = False
        badchars = self.state.settings.get("badchars")
        split_sc = self.state.sc.split(self.state.settings.get("hex_prefix"))[1:]

        print(white("Built shellcode :", bold=True))
        print(self.state.sc)
        print()

        print(white("Length :", bold=True))
        print(sc_len(self.state.sc, prefix=self.state.settings.get("hex_prefix")))
        print()

        uniq_sc = [b for b in sorted(list(set(split_sc)), key=lambda b: int(b, 16)) if
                   b != ""]  # We don't want to filter out values such as 0

        hasher = hashlib.md5()
        hasher.update(self.state.sc.encode())
        md5 = hasher.hexdigest()
        print(f"{white('MD5 : ', bold=True)}{md5}")

        hasher = hashlib.sha1()
        hasher.update(self.state.sc.encode())
        sha1 = hasher.hexdigest()
        print(f"{white('SHA1 : ', bold=True)}{sha1}")

        hasher = hashlib.sha256()
        hasher.update(self.state.sc.encode())
        sha256 = hasher.hexdigest()
        print(f"{white('SHA256 : ', bold=True)}{sha256}")

        print()

        print(white("Unique bytes :", bold=True))
        print(" ".join(uniq_sc))
        print()

        print(white("Badchars :", bold=True))
        print(" ".join(self.state.settings.get("badchars")))

        for bc in badchars:
            for idx, b in enumerate(split_sc):
                if bc == b:
                    bc_found = True
                    split_sc[idx] = str(red(split_sc[idx], bold=True))

        print()
        if bc_found:
            print(white("Badchars found !", bold=True))
            print(" ".join(split_sc))
        else:
            print(f"Badchars {green('OK', bold=True)}")

    def do_encode(self, line):
        usage = "Usage : encode <shellcode>"
        if not self.state.encoder:
            print("Select an encoder first")
            return
        elif not len(line) > 0:
            print(usage)
            return

        shellcode = line[0]

        encoded = self.state.encoder.encode(shellcode, self.state.settings.get("hex_prefix"))
        print(hex_to_escaped_string(encoded, self.state.settings.get("hex_prefix")))

    def do_decode(self, line):
        usage = "Usage : decode <shellcode>"
        if not self.state.encoder:
            print("Select an encoder first")
            return
        elif not len(line) > 0:
            print(usage)
            return

        shellcode = line[0]
        decoded = self.state.encoder.decode(shellcode, self.state.settings.get("hex_prefix"))
        print(hex_to_escaped_string(decoded, self.state.settings.get("hex_prefix")))

    def do_render(self, line):
        no_encoder = False

        if not self.state.encoder:
            print("Encoder not selected, continuing with raw payload")
            no_encoder = True

        if not self.state.payload:
            print("Select a payload first")
            return

        badchars = self.state.settings.get("badchars")
        badchars_max_loop = self.state.settings.get("badchars_max_loop")
        rendered = ""

        try:
            if self.state.settings.get("no_badchars"):
                for i in range(0, badchars_max_loop + 1):
                    rendered = self.state.payload.render()
                    split_sc = rendered.split(self.state.settings.get("hex_prefix"))[1:]
                    if not has_badchars(badchars, split_sc):
                        break
                    if i == badchars_max_loop - 1:
                        print(f"Failed to get a payload shellcode without any badchars after {i} iterations")
                        return

                    print(f"({i}/{badchars_max_loop}) Failed to generate a shellcode without any badchars")
            else:
                rendered = self.state.payload.render()
        except (MissingRequired, UnsupportedArch) as e:
            print(f"Error while patching payload : {e}")
            return

        if not no_encoder:
            try:
                if self.state.settings.get("no_badchars"):
                    for i in range(0, badchars_max_loop + 1):
                        rendered = self.state.encoder.render(self.state.payload)
                        print(rendered)
                        split_sc = rendered.split(self.state.settings.get("hex_prefix"))[1:]
                        if not has_badchars(badchars, split_sc):
                            break
                        if i == badchars_max_loop:
                            print(f"Failed to get a decoder shellcode without any badchars after {i} iterations")
                            return

                        print(f"({i}/{badchars_max_loop}) Failed to generate a shellcode without any badchars")
                else:
                    rendered = self.state.encoder.render(self.state.payload)
            except (MissingRequired, UnsupportedArch) as e:
                print(f"Error while patching payload : {e}")
                return

        if rendered:
            print(rendered)
            if self.state.settings.get("clipboard"):
                pyperclip.copy(rendered)
                print("Shellcode has been copied to clipboard")

        self.state.sc = rendered

    def do_compile(self, line):
        usage = "compile <path/to/file>"
        if len(line) < 1:
            print(usage)

            return

        path = os.path.abspath(line[0])
        out, nasm_out, err = compile_path_to_shellcode(path, self.state.settings.get("arch"), self.state.settings.get("optimization_level"),
                                                       clipboard=self.state.settings.get("clipboard"))

        if out:
            print(out)

        if nasm_out:
            print(nasm_out)

        if err:
            print(err.decode())

    def do_search(self, line):
        usage = "Usage : search [encoder|payload|script] <term>"
        if not len(line) > 1:
            print(usage)
            return
        elif line[0] not in ["encoder", "payload", "script"]:
            print("Invalid category (encoder, payload, script)")
            return

        category = line[0]
        term = line[1]
        found = []

        for path in getattr(self.state, f"loaded_{category}"):
            if term in path:
                found.append(path.replace(term, str(red(term, bold=True))))

        print("\n".join(found))

    def do_len(self, line):
        usage = "Usage : len <shellcode>"
        if not len(line) > 0:
            print(usage)
            return

        sc = line[0]
        print(sc_len(sc))

    def do_run(self, line):
        self.do_try(line)

    def do_try(self, line):
        self.do_render(line)
        if not self.state.sc:
            print("Use \"render\" first to generate the shellcode")
            return

        # no_encoder = False
        #
        # if len(line) > 0:
        #     sc = line[0]
        #
        #     # if not arch == "32" and not arch == "64":
        #     #     print(f"Unsupported arch : {arch} [32/64]")
        #     #     return
        #
        #     compile_and_run(self.state.settings, sc)
        #     return
        #
        # if not self.state.encoder:
        #     print("Encoder not selected, continuing with raw payload")
        #     no_encoder = True
        #
        # if not self.state.payload:
        #     print("Select a payload first")
        #     return
        #
        # try:
        #     rendered = self.state.payload.render()
        # except (MissingRequired, UnsupportedArch) as e:
        #     print(f"Error while patching payload : {e}")
        #     return
        #
        # try:
        #     if not no_encoder:
        #         rendered = self.state.encoder.render(self.state.payload)
        # except (MissingRequired, UnsupportedArch) as e:
        #     print(f"Error while patching encoder : {e}")
        #     return
        #
        # print(f"Trying {rendered}")
        print(f"Trying {self.state.sc}")

        compile_and_run(self.state.settings, self.state.sc)

    def do_script(self, line):
        usage = "Usage : script <path>"
        if not len(line) > 0:
            print(usage)
            return

        path = line[0]
        with open(path, "r") as f:
            lines = f.readlines()

        for line in lines:
            if line.startswith("#"):
                continue
            print(white("(orgasm)> " + line.strip(), bold=True))
            self.process(line.strip())
            print()
