import importlib.util
from pathlib import Path


def load_plugin(path, category):
    spec = importlib.util.spec_from_file_location("plugin", path)
    plugin_class = f"{Path(path).stem.title()}{category.title()}".replace('_', '')
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    plugin = getattr(mod, plugin_class)

    return plugin
