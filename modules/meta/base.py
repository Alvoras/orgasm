import random

from lib.exception import UnsupportedArch


class BasePlugin:
    def __init__(self):
        self.alterations = {}

    def get_random_alt(self, arch, reg1="", reg2="", val1="", val2="", val3="", val4=""):
        alterations = self.alterations.get(str(arch))

        if not alterations:
            raise UnsupportedArch

        rng = random.randint(0, len(alterations)-1)
        alt = alterations[rng]

        for idx, line in enumerate(alt):
            line = line.replace("<reg1>", str(reg1))
            line = line.replace("<reg2>", str(reg2))
            line = line.replace("<val1>", str(val1))
            line = line.replace("<val2>", str(val2))
            line = line.replace("<val2>", str(val3))
            line = line.replace("<val2>", str(val4))
            alt[idx] = line
        return "\n".join(alt)
