from modules.meta.base import BasePlugin


class Junk(BasePlugin):
    def __init__(self):
        super().__init__()
        self.name = "junk"
        self.alterations = {
            "32":
                [
                    ["push edi", "pop edi"],
                    ["inc esi", "dec esi"],
                    ["nop"]
                ],
            "64": [
                ["push rdi", "pop rdi"],
                ["inc esi", "dec esi"],
                ["nop"]
            ]
        }
