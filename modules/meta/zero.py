from modules.meta.base import BasePlugin


class Zero(BasePlugin):
    def __init__(self):
        super().__init__()
        self.name = "zero"
        self.alterations = {
            "32": [
                [f"xor <reg1>, <reg1>"],
                [f"mov <reg1>, 0"],
                [f"sub <reg1>, <reg1>"],
            ],
            "64": [
                [f"xor <reg1>, <reg1>"],
                [f"mov <reg1>, 0"],
                [f"sub <reg1>, <reg1>"],
            ]
        }
