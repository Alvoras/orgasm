import re
import random

from lib.exception import MarkerNotFound


class MetaEngine:

    def __init__(self):
        self.name = "base"
        self.alterations = {}
        self.plugins = {}

    def add(self, plugin):
        eng = plugin()
        self.plugins[eng.name] = eng

    def apply(self, name, arch, **kwargs):
        if not (plugin := self.plugins.get(name)):
            print(f"Metamorphic plugin not found \"{name}\"")
            return ""

        return plugin.get_random_alt(arch, reg1=kwargs.get("reg1"), reg2=kwargs.get("reg2"), val1=kwargs.get("val1"),
                                     val2=kwargs.get("val2"), val3=kwargs.get("val3"), val4=kwargs.get("val4"))

    def mark(self, name, **kwargs):
        return f"{name}({','.join(key + '=' + val for key, val in kwargs.items())})"

    def extract_args(self, line, name):
        try:
            found = re.search(f"{name}\((.+?\))", line).group(1)
        except AttributeError:
            raise MarkerNotFound(name)

        return [arg.strip(" ") for arg in found.split(",")]
