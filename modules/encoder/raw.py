from modules.encoder.base import BaseEncoder
from lib.utils import escape_hex_string


class RawEncoder(BaseEncoder):
    def __init__(self, state):
        super().__init__(state)
        self.name = "Raw"

    def decoder_32(self):
        # Prepend NOP to the payload
        return "90"

    def decoder_64(self):
        # Prepend NOP to the payload
        return "90"
