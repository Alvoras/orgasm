from lib.options import Options
from lib.utils import escaped_string_to_hex, hex_to_escaped_string, validate_sc_template, escape_hex_string, \
    compile_source_to_shellcode
from lib.exception import UnsupportedArch, MissingRequired, UnsupportedPatcherInput, InvalidShellcode


class BaseEncoder:
    def __init__(self, state):
        # self.patch_shellcode[arch] = False
        self.options = Options()
        for opt in state.settings.get_options():
            if not opt.scoped and opt.public:
                self.options.add_from_option(opt)

        # self.options.add("hex_prefix", value="\\x", required=True)
        self.sc_template = {
            "32": fr"",
            "64": fr""
        }

        self.source_template = {
            "32": fr"",
            "64": fr""
        }

        self.patch_shellcode = {
            "32": False,
            "64": False
        }

        self.sc_template_hex_prefix = ""

        self.patchers = []
        self.metas = []

    def get_options(self):
        return self.options

    def encode(self, sc, hex_prefix):
        encoded = []
        sc = sc.strip("\"")
        # hex_prefix = self.sc_template_hex_prefix

        try:
            if hex_prefix != "" and hex_prefix in sc:
                hex_sc = escaped_string_to_hex(sc, prefix=hex_prefix)
            else:
                hex_sc = bytearray.fromhex(sc)
        except ValueError:
            raise InvalidShellcode(sc)

        for op in hex_sc:
            encoded.append(self.encode_byte(op))

        return encoded

    def decode(self, sc, hex_prefix):
        decoded = []
        sc = sc.strip("\"")

        try:
            if hex_prefix in sc:
                hex_sc = escaped_string_to_hex(sc, prefix=hex_prefix)
            else:
                hex_sc = bytearray.fromhex(sc)
        except ValueError:
            raise InvalidShellcode(sc)

        for op in hex_sc:
            decoded.append(self.decode_byte(op))

        return decoded

    def render(self, payload):
        self.register_templates()
        arch = self.options.get("arch")

        try:
            self.check_arch()
            self.options.check_required()
        except (MissingRequired, UnsupportedArch) as e:
            raise e

        rendered_payload = payload.render()
        # Encode payload's shellcode with the user defined offset
        try:
            encoded_sc = self.encode(rendered_payload, self.sc_template_hex_prefix)
        except InvalidShellcode as e:
            print(f"Error while encoding shellcode : {e}")
            return

        encoded_payload = hex_to_escaped_string(encoded_sc, prefix="")
        try:
            # Patch decoder shellcode with the parameters of the newly encoded shellcode
            patched_decoder = self.patch_decoder(encoded_payload)
        except UnsupportedArch:
            print(f"Error while patching decoder : unsupported arch ({arch})")
            return

        sc_decoder = patched_decoder

        if not self.patch_shellcode[arch]:
            sc_decoder, nasm_out, err = compile_source_to_shellcode(patched_decoder, arch, self.options.get("optimization_level"), hex_prefix="")

            if nasm_out:
                print(nasm_out)

            if err:
                print("Error while compiling source code of decoder :", err)
                return

            sc_decoder = sc_decoder.strip()

        return escape_hex_string(sc_decoder, prefix=self.options.get("hex_prefix")) + escape_hex_string(encoded_payload,
                                                                                                        prefix=self.options.get(
                                                                                                            "hex_prefix"))

    def encode_byte(self, op):
        return op

    def decode_byte(self, op):
        return op

    def add_patcher(self, patcher):
        self.patchers.append(patcher)

    def add_meta(self, meta):
        self.metas.append(meta)

    def decoder_32(self):
        return ""

    def decoder_64(self):
        return ""

    def register_templates(self):
        if template := self.decoder_32():
            if template.count("\n") > 2:
                self.set_decoder_32_from_source(template)
            else:
                self.set_decoder_32(template)

        if template := self.decoder_64():
            if template.count("\n") > 2:
                self.set_decoder_64_from_source(template)
            else:
                self.set_decoder_64(template)

    def set_decoder_32(self, template):
        self.patch_shellcode["32"] = True
        self.sc_template_hex_prefix = r"\x"
        self.set_decoder(template.replace(self.sc_template_hex_prefix, ""), "32")

    def set_decoder_64(self, template):
        self.patch_shellcode["64"] = True
        self.sc_template_hex_prefix = r"\x"
        self.set_decoder(template.replace(self.sc_template_hex_prefix, ""), "64")

    def set_decoder_32_from_source(self, template):
        self.patch_shellcode["32"] = False
        self.set_decoder_from_source(template, "32")

    def set_decoder_64_from_source(self, template):
        self.patch_shellcode["64"] = False
        self.set_decoder_from_source(template, "64")

    def set_decoder(self, template, arch):
        template = template.replace(self.options.get("hex_prefix"), "")
        err, valid = validate_sc_template(template)
        if not valid:
            print("Invalid template :", err)

        self.sc_template[arch] = template

    def set_decoder_from_source(self, template, arch):
        self.source_template[arch] = template

    def patch_decoder(self, encoded_payload):
        template = self.get_template()
        arch = self.options.get("arch")

        # if not self.patch_shellcode[arch]:
        #     for engine in self.metas:
        #         template = engine(template).apply(self.options)

        for patcher in self.patchers:
            try:
                if patcher.token in template:
                    template = patcher(template, self.patch_shellcode[arch]).apply(self.options, encoded_payload)
            except UnsupportedPatcherInput as e:
                print(f"Error while patching decoder : {e}")

        return template

    def get_template(self):
        arch = self.options.get("arch")

        self.check_arch()
        if self.patch_shellcode[arch]:
            return self.sc_template[arch]
        else:
            return self.source_template[arch]

    def check_arch(self):
        arch = self.options.get("arch")

        if self.patch_shellcode[arch]:
            if not bool(self.sc_template[arch]):
                raise UnsupportedArch(arch)
        else:
            if not bool(self.source_template[arch]):
                raise UnsupportedArch(arch)
    # def get_decoder(self):
    #     arch = self.options.get("arch")
    #     return self.sc_template[arch]
