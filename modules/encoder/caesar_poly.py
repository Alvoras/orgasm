from modules.encoder.base import BaseEncoder
from modules.meta.zero import Zero
from modules.meta.metaengine import MetaEngine
from modules.meta.junk import Junk
from modules.patcher.shellcode_len import ShellcodeLen
from modules.patcher.offset import Offset


class CaesarPolyEncoder(BaseEncoder):
    def __init__(self, state):
        super().__init__(state)
        self.name = "Caesar polymorphic"
        self.options.add_patcher_options(Offset)

        self.add_patcher(Offset)
        self.add_patcher(ShellcodeLen)

    def decoder_32(self):
        meta = MetaEngine()
        meta.add(Junk)
        meta.add(Zero)

        return rf"""
global _start 

section .text

_start:
   jmp    one
   {meta.apply('junk', 32)}

four:
   pop    esi
   {meta.apply('zero', 32, reg1='ecx')}

   mov    cl,{ShellcodeLen.token}
two:
   sub    BYTE [esi+ecx-1],{Offset.token}
   sub    cl,1
   jne    two
   jmp    three
one:
   call   four
three:
"""

    def decoder_64(self):
        return rf"""
[SECTION .text]

global _start:

_start:
   jmp    one

four:
   pop    rsi

   xor    rcx, rcx
   mov    cl,{ShellcodeLen.token}
two:
   sub    BYTE [rsi+rcx-1],{Offset.token}
   sub    cl,0x1
   jne    two
   jmp    three
one:
   call   four

three:
"""

    def encode_byte(self, op):
        # return op + self.options.get("offset") % 26
        return int(format(op + self.options.get("offset") % 26, "02x")[-2:], 16)

    def decode_byte(self, op):
        # return op - self.options.get("offset") % 26
        return int(format(op - self.options.get("offset") % 26, "02x")[-2:], 16)
