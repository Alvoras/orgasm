from modules.encoder.base import BaseEncoder
from modules.patcher.shellcode_len import ShellcodeLen
from modules.patcher.offset import Offset


class CaesarEncoder(BaseEncoder):
    def __init__(self, state):
        super().__init__(state)
        self.name = "Caesar"
        self.options.add_patcher_options(Offset)

        self.add_patcher(Offset)
        self.add_patcher(ShellcodeLen)

    def decoder_32(self):
        return fr"\xeb\x11\x5e\x31\xc9\xb1{ShellcodeLen.token}\x80\x6c\x0e\xff{Offset.token}\x80\xe9\x01\x75\xf6\xeb\x05\xe8\xea\xff\xff\xff"

    def encode_byte(self, op):
        return op + self.options.get("offset") % 26

    def decode_byte(self, op):
        return op - self.options.get("offset") % 26
