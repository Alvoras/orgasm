from lib.utils import validate_sc_template, escape_hex_string, compile_source_to_shellcode
from lib.options import Options, Option
from lib.exception import UnsupportedArch, MissingRequired


class BasePayload:
    def __init__(self, state):
        self.name = "base"
        self.sc_template = {
            "32": fr"",
            "64": fr""
        }

        self.source_template = {
            "32": fr"",
            "64": fr""
        }

        self.patch_shellcode = {
            "32": False,
            "64": False
        }

        self.sc_template_hex_prefix = ""

        self.options = Options()
        for opt in state.settings.get_options():
            if not opt.scoped and opt.public:
                self.options.add_from_option(opt)

        self.patchers = []

    # def set_payload_32(self, template, hex_prefix=""):
    #     self.set_payload(template.replace(hex_prefix, ""), "32")
    #
    # def set_payload_64(self, template, hex_prefix=""):
    #     self.set_payload(template.replace(hex_prefix, ""), "64")
    #
    # def set_payload(self, template, arch):
    #     template = template.replace(self.options.get("hex_prefix"), "")
    #     err, valid = validate_sc_template(template)
    #     if not valid:
    #         print("Invalid shellcode template :", err)
    #
    #     self.template[arch] = template

    def payload_32(self):
        return ""

    def payload_64(self):
        return ""

    def get_template(self):
        arch = self.options.get("arch")

        self.check_arch()
        if self.patch_shellcode[arch]:
            return self.sc_template[arch]
        else:
            return self.source_template[arch]

    def register_templates(self):
        if template := self.payload_32():
            if template.count("\n") > 2:
                self.set_payload_32_from_source(template)
            else:
                self.set_payload_32(template)

        if template := self.payload_64():
            if template.count("\n") > 2:
                self.set_payload_64_from_source(template)
            else:
                self.set_payload_64(template)

    def set_payload_32(self, template):
        self.patch_shellcode["32"] = True
        self.sc_template_hex_prefix = r"\x"
        self.set_payload(template.replace(self.sc_template_hex_prefix, ""), "32")

    def set_payload_64(self, template):
        self.patch_shellcode["64"] = True
        self.sc_template_hex_prefix = r"\x"
        self.set_payload(template.replace(self.sc_template_hex_prefix, ""), "64")

    def set_payload_32_from_source(self, template):
        self.patch_shellcode["32"] = False
        self.set_payload_from_source(template, "32")

    def set_payload_64_from_source(self, template):
        self.patch_shellcode["64"] = False
        self.set_payload_from_source(template, "64")

    def set_payload(self, template, arch):
        template = template.replace(self.options.get("hex_prefix"), "")
        err, valid = validate_sc_template(template)
        if not valid:
            print("Invalid template :", err)

        self.sc_template[arch] = template

    def set_payload_from_source(self, template, arch):
        self.source_template[arch] = template

    # def get_template(self, arch):
    #     try:
    #         return self.template[arch]
    #     except KeyError:
    #         raise UnsupportedArch(arch)

    def add_patcher(self, patcher):
        self.patchers.append(patcher)

    def render(self):
        self.register_templates()
        try:
            self.check_arch()
            self.options.check_required()
        except (MissingRequired, UnsupportedArch) as e:
            raise e

        return self.patch_payload()

    def patch_payload(self):
        arch = self.options.get("arch")
        template = self.get_template()

        for patcher in self.patchers:
            if patcher.token in template:
                template = patcher(template, patch_shellcode=self.patch_shellcode[arch]).apply(self.options, "")

        sc_payload = template

        if not self.patch_shellcode[arch]:
            sc_payload, nasm_out, err = compile_source_to_shellcode(template, arch, self.options.get("optimization_level"), hex_prefix="")

            if nasm_out:
                print(nasm_out)

            if err:
                print("Error while compiling source code of decoder :", err)
                return

            sc_payload = sc_payload.strip()

        # return escape_hex_string(sc_payload, prefix=self.options.get("hex_prefix"))
        return sc_payload

    def check_arch(self):
        arch = self.options.get("arch")
        if self.patch_shellcode[arch]:
            if not bool(self.sc_template[arch]):
                raise UnsupportedArch(arch)
        else:
            if not bool(self.source_template[arch]):
                raise UnsupportedArch(arch)
