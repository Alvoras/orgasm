from modules.payload.base import BasePayload
from lib.exception import UnsupportedArch, MissingRequired


class RawPayload(BasePayload):
    def __init__(self, state):
        super().__init__(state)
        self.name = "Raw"
        self.options.add("shellcode", value="", type=str, required=True)

    def render(self):
        try:
            self.options.check_required()
        except (MissingRequired, UnsupportedArch) as e:
            raise e

        return self.options.get("shellcode")
