from lib.options import Options


class BasePatcher:
    options = Options()
    token = ""

    def __init__(self, sc, patch_shellcode=True):
        self.sc = sc
        # self.token = ""
        self.patch_shellcode = patch_shellcode

    def apply(self, options, encoded_payload):
        return self.sc

    def patch_with(self, asm):
        # self.sc.replace(self.token, asm)
        return self.sc.replace(self.token, asm)

    @classmethod
    def get_options(cls):
        return cls.options
