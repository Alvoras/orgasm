from modules.patcher.base import BasePatcher
from lib.options import Options


class Offset(BasePatcher):
    token = "$OFFSET$"
    options = Options()
    options.add("offset", value=13, change_me=False, type=int, required=True)

    def __init__(self, sc, patch_shellcode):
        super().__init__(sc, patch_shellcode)

    def apply(self, options, base_payload):
        if self.patch_shellcode:
            return self.patch_with(format(options.get("offset"), "02x"))

        return self.patch_with(format(options.get("offset"), "02x") + "h")
