from lib.exception import UnsupportedPatcherInput
from modules.patcher.base import BasePatcher
from lib.options import Options
from lib.opcode import memory32, memory64


class AsciiData(BasePatcher):
    token = "$ASCII_DATA$"
    options = Options()
    options.add("data", required=True)

    def __init__(self, sc, patch_shellcode):
        super().__init__(sc, patch_shellcode)

    def apply(self, options, encoded_payload):
        if not self.patch_shellcode:
            # raise UnsupportedPatcherInput("AsciiData", "source")
            asm = ""
            data = options.get("data")
            if options.get("arch") == "32":
                chunks = [data[i:i+4] for i in range(0, len(data), 4)]
                for chunk in chunks:
                    asm = f"push dword \"{chunk}\"\n" + asm
            else:
                chunks = [data[i:i+8] for i in range(0, len(data), 8)]
                for chunk in chunks:
                    asm = f"mov rdi, \"{chunk}\"\n    push rdi\n" + asm
        else:
            asm = ""
            padding = memory64.NOP[0] if options.get("arch") == "64" else memory32.NOP[0]
            data = bytearray(options.get("data"), "utf-8")

            if options.get("arch") == "32":
                chunks = [data[i:i+4] for i in range(0, len(data), 4)]
                for chunk in chunks:
                    while len(chunk) < 4:
                        chunk.insert(len(chunk), padding)
                    chunk.insert(0, memory32.PUSH[0])
                    asm = ''.join(format(x, "02x") for x in chunk) + asm
            elif options.get("arch") == "64":
                chunks = [data[i:i+8] for i in range(0, len(data), 8)]
                for chunk in chunks:
                    while len(chunk) < 8:
                        chunk.insert(len(chunk), padding)
                    chunk.insert(0, memory64.MOVABS[1])
                    chunk.insert(0, memory64.MOVABS[0])
                    asm = ''.join(format(x, "02x") for x in chunk) + asm

        return self.patch_with(asm)
