from modules.patcher.base import BasePatcher


class ShellcodeLen(BasePatcher):
    token = "$SC_LEN$"

    def __init__(self, sc, patch_shellcode):
        super().__init__(sc, patch_shellcode)

    def apply(self, options, encoded_payload):
        if self.patch_shellcode:
            return self.patch_with(format(len(bytearray.fromhex(encoded_payload)), "02x"))

        return self.patch_with(format(len(bytearray.fromhex(encoded_payload)), "02x") + "h")
