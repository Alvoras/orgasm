from modules.patcher.base import BasePatcher


class AsciiLen(BasePatcher):
    token = "$ASCII_LEN$"

    def __init__(self, sc, patch_shellcode):
        super().__init__(sc, patch_shellcode)

    def apply(self, options, encoded_payload):
        # Add 1 for string terminator \0
        if self.patch_shellcode:
            return self.patch_with(format(len(options.get("data")), "02x"))

        return self.patch_with(format(len(options.get("data")), "02x") + "h")
