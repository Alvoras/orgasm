#!/usr/bin/env python3.8

import sys
import pyperclip
import argparse

from prompt_toolkit import PromptSession
from prompt_toolkit.completion import NestedCompleter
from prompt_toolkit.styles import Style
from prompt_toolkit.history import FileHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory

from pygments.lexers import load_lexer_from_file

from lib.orgasm import Orgasm
from lib.state import State
from lib.utils import (
    check_clipboard,
    print_loaded
)


def main():
    header_title = """    ,-----.    .-------.      .-_'''-.      ____       .-'''-. ,---.    ,---. 
  .'  .-,  '.  |  _ _   \    '_( )_   \   .'  __ `.   / _     \|    \  /    | 
 / ,-.|  \ _ \ | ( ' )  |   |(_ o _)|  ' /   '  \  \ (`' )/`--'|  ,  \/  ,  | 
;  \  '_ /  | :|(_ o _) /   . (_,_)/___| |___|  /  |(_ o _).   |  |\_   /|  | 
|  _`,/ \ _/  || (_,_).' __ |  |  .-----.   _.-`   | (_,_). '. |  _( )_/ |  | 
: (  '\_/ \   ;|  |\ \  |  |'  \  '-   .'.'   _    |.---.  \  :| (_ o _) |  | 
 \ `"/  \  ) / |  | \ `'   / \  `-'`   | |  _( )_  |\    `-'  ||  (_,_)  |  | 
  '. \_/``".'  |  |  \    /   \        / \ (_ o _) / \       / |  |      |  | 
    '-----'    ''-'   `'-'     `'-...-'   '.(_,_).'   `-...-'  '--'      '--'     
"""
    completer = NestedCompleter.from_nested_dict(
        {
            "use": {
                "encoder": {},
                "payload": {},
            },
            "set": {},
            "options": {},
            "help": {},
            "search": {
                "encoder": {},
                "payload": {},
                "script": {},
            },
            "encode": {},
            "decode": {},
            "render": {
                "encoder": {},
                "payload": {},
            },
            "len": {},
            "script": {}
        }
    )

    style = Style.from_dict({
        'completion-menu.completion': 'bg:#008888 #ffffff',
        'completion-menu.completion.current': 'bg:#00aaaa #000000',
        'scrollbar.background': 'bg:#88aaaa',
        'scrollbar.button': 'bg:#222222',
    })

    header = """
L'astucieux Monsieur Malice
spécialiste en assistance
aux Maniganceurs de Mauvais Coups
est fier de vous présenter
"""

    state = State(completer)

    history = FileHistory(".orgasm_history")
    lexer = load_lexer_from_file("lib/lexer.py", "OrgasmLexer", **{})
    session = PromptSession(history=history,
                            auto_suggest=AutoSuggestFromHistory(),
                            enable_history_search=True,
                            lexer=lexer,
                            completer=state.completer,
                            style=style)

    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--execute", dest="script", default="", help="Execute a script and exit")
    args = parser.parse_args()

    if args.script:
        check_clipboard()
        orgasm = Orgasm(session, state)
        orgasm.exec(args.script)
    else:
        for line in header.split("\n"):
            print(line.center(80, " "))
        print(header_title)

        if check_clipboard():
            state.settings.set("clipboard", True)

        print_loaded(state)
        orgasm = Orgasm(session, state)

        orgasm.run()


if __name__ == '__main__':
    sys.path.insert(0, "modules/encoder")
    sys.path.insert(0, "modules/payload")
    main()
