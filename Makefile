default: help

## run : Start OrgASM
run:
	./venv/bin/python ./main.py

## install : Create the virtual env and install requirements
install: venv install_reqs

## venv : Create a blank virtual environment
venv:
	virtualenv venv --python=$(which python3)

## install_reqs : Install pip packages
install_reqs:
	./venv/bin/pip install -r ./requirements.txt

## req : Freeze pyhton requirements
req:
	./venv/bin/pip freeze | grep -v "pkg-resources" > requirements.txt

help: Makefile
		@printf "\n OrgASM\n\n"
		@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
		@printf ""
